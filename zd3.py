"""
3. Создайте собственный класс-исключение, который должен проверять содержимое списка на наличие только чисел.
Проверить работу исключения на реальном примере. Необходимо запрашивать у пользователя данные и заполнять список
только числами. Класс-исключение должен контролировать типы данных элементов списка.
"""


class ListValueException(Exception):
    def __init__(self, txt):
        self.txt = txt


result_list: list = list()
while True:
    element = input("Введите новый элемет списка или \"stop\" >>> ")
    if element.lower() == "stop":
        break
    try:
        if not element.isdigit():
            raise ListValueException("Введено не число")
        result_list.append(int(element))
    except ListValueException as lve:
        print(lve)

print(result_list)
