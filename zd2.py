"""
2. Создайте собственный класс-исключение, обрабатывающий ситуацию деления на нуль. Проверьте его работу на данных,
вводимых пользователем. При вводе пользователем нуля в качестве делителя программа должна корректно обработать
эту ситуацию и не завершиться с ошибкой.
"""


class MyZeroDivisionException(Exception):
    def __init__(self, txt):
        self.txt = txt


a: int = 5
b: int = 0

try:
    if b == 0:
        raise MyZeroDivisionException("Деление на нуль!")
    else:
        a / b
except MyZeroDivisionException as err:
    print(err)

