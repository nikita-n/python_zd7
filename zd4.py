"""
4. Начните работу над проектом «Склад оргтехники». Создайте класс, описывающий склад. А также класс «Оргтехника»,
который будет базовым для классов-наследников. Эти классы — конкретные типы оргтехники (принтер, сканер, ксерокс).
В базовом классе определить параметры, общие для приведенных типов. В классах-наследниках реализовать параметры,
уникальные для каждого типа оргтехники.
"""

from __future__ import annotations
from enum import Enum


class Storage:
    pass


class OfficeEquipment:
    title: str
    price: float

    def __init__(self, title: str, price: float):
        self.title = title
        self.price = price


class Printer(OfficeEquipment):
    class _PrinterType(Enum):
        monochrome = 1
        color = 2

    printer_type: _PrinterType

    def __init__(self, title: str, price: float, printer_type: _PrinterType):
        super().__init__(title, price)
        self.printer_type = printer_type


class Scanner(OfficeEquipment):
    scan_in_min: int

    def __init__(self, title: str, price: float, scan_in_min: int):
        super().__init__(title, price)
        self.scan_in_min = scan_in_min



